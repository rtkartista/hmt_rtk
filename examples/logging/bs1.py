# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2014 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
"""
Simple example that connects to the first Crazyflie found, logs the Stabilizer
and prints it to the console. After 10s the application disconnects and exits.
"""
import logging
import time
from threading import Timer
import numpy as np

import cflib.crtp  # noqa
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.utils import uri_helper

uri = uri_helper.uri_from_env(default='radio://0/100/2M/E7E7E7E705')

# Only output errors from the logging framework
logging.basicConfig(level=logging.ERROR)


class LoggingExample:
    """
    Simple logging example class that logs the Stabilizer from a supplied
    link uri and disconnects after 5s.
    """

    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """

        self._cf = Crazyflie(rw_cache='./cache')

        # Connect some callbacks from the Crazyflie API
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        print('Connecting to %s' % link_uri)

        # Try to connect to the Crazyflie
        self._cf.open_link(link_uri)
        self.arr = []

        # Variable used to keep main loop occupied until disconnect
        self.is_connected = True

    def _connected(self, link_uri):
        log_rate = 10
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        print('Connected to %s' % link_uri)

        # The definition of the logconfig can be made before connecting
        self._lg_stab = LogConfig(name='Stabilizer', period_in_ms=100)
        self._lg_stab.add_variable('kalman.stateX', 'float')
        """self._lg_stab.add_variable('stabilizer.y', 'float')
        self._lg_stab.add_variable('stabilizer.z', 'float')
        self._lg_stab.add_variable('stabilizer.roll', 'float')
        self._lg_stab.add_variable('stabilizer.pitch', 'float')
        self._lg_stab.add_variable('stabilizer.yaw', 'float')"""
        # The fetch-as argument can be set to FP16 to save space in the log packet
        """self._lg_stab.add_variable('pm.vbat', 'FP16')"""

        range_log_conf1 = LogConfig("seq0-5", log_rate) # sequence numbers 0-5
        range_log_conf2 = LogConfig("seq6-7", log_rate) # sequence numbers 6-7

        range_log_conf3 = LogConfig("distance difference 0-5", log_rate) # distance differece 0-5
        range_log_conf4 = LogConfig("distance difference 6-7", log_rate) # distance difference 6-7

        range_log_conf5 = LogConfig("timestamp0-5", log_rate) # timestamps 0-5
        range_log_conf6 = LogConfig("timestamp6-7", log_rate) # timestamps 6-7

        range_log_conf1.add_variable('tdoa2new.sn-0', fetch_as="float")
        range_log_conf1.add_variable('tdoa2new.sn-1', fetch_as="float")
        range_log_conf1.add_variable('tdoa2new.sn-2', fetch_as="float")
        range_log_conf1.add_variable('tdoa2new.sn-3', fetch_as="float")
        range_log_conf1.add_variable('tdoa2new.sn-4', fetch_as="float")
        range_log_conf1.add_variable('tdoa2new.sn-5', fetch_as="float")
        range_log_conf2.add_variable('tdoa2new.sn-6', fetch_as="float")
        range_log_conf2.add_variable('tdoa2new.sn-7', fetch_as="float")


        range_log_conf3.add_variable('tdoa.d0-1', fetch_as="float")
        range_log_conf3.add_variable('tdoa.d1-2', fetch_as="float")
        range_log_conf3.add_variable('tdoa.d2-3', fetch_as="float")
        range_log_conf3.add_variable('tdoa.d3-4', fetch_as="float")
        range_log_conf3.add_variable('tdoa.d4-5', fetch_as="float")
        range_log_conf3.add_variable('tdoa.d5-6', fetch_as="float")
        range_log_conf4.add_variable('tdoa.d6-7', fetch_as="float")
        range_log_conf4.add_variable('tdoa.d7-0', fetch_as="float")


        range_log_conf5.add_variable('tdoa2new.ts-0', fetch_as="float")
        range_log_conf5.add_variable('tdoa2new.ts-1', fetch_as="float")
        range_log_conf5.add_variable('tdoa2new.ts-2', fetch_as="float")
        range_log_conf5.add_variable('tdoa2new.ts-3', fetch_as="float")
        range_log_conf5.add_variable('tdoa2new.ts-4', fetch_as="float")
        range_log_conf5.add_variable('tdoa2new.ts-5', fetch_as="float")
        range_log_conf6.add_variable('tdoa2new.ts-6', fetch_as="float")
        range_log_conf6.add_variable('tdoa2new.ts-7', fetch_as="float")

        # For looping
        self.log_configs = [\
            range_log_conf1,\
            range_log_conf2,\
            #range_log_conf3,\
            #range_log_conf4,\
            #range_log_conf5,\
            #range_log_conf6,\
        ]

        # Adding the configuration cannot be done until a Crazyflie is
        # connected, since we need to check that the variables we
        # would like to log are in the TOC.
        for conf in self.log_configs:
            self._cf.log.add_config(conf)
            # This callback will receive the data
            conf.data_received_cb.add_callback(self._stab_log_data)
            # This callback will be called on errors
            conf.error_cb.add_callback(self._stab_log_error)
            # Start the logging
            conf.start()
        
        # Start a timer to disconnect in 10s
        t = Timer(0.01, self._cf.close_link)
        t.start()
        

    def _stab_log_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print('Error when logging %s: %s' % (logconf.name, msg))

    def _stab_log_data(self, timestamp, data, logconf):
        """Callback from a the log API when data arrives"""
        #print(f'[{timestamp}][{logconf.name}]: ', end='')
        for name, value in data.items():
            print(f'{name}: {value:3.3f} ', end='')
            self.arr.append([name, "   ", value])
            with open('pos1-logging.csv','a') as csvfile:
                np.savetxt(csvfile, self.arr, delimiter=',', fmt='%s', comments='')

        print("logging...")

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the specified address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)
        self.is_connected = False


if __name__ == '__main__':
    # Initialize the low-level drivers
    cflib.crtp.init_drivers()

    le = LoggingExample(uri)

    # The Crazyflie lib doesn't contain anything to keep the application alive,
    # so this is where your application should do something. In our case we
    # are just waiting until we are disconnected.
    while le.is_connected:
        time.sleep(1)
