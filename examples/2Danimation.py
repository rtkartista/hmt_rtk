from matplotlib import dates as mdate
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import pandas as pd
import numpy as np
import pdb

repo = pd.read_csv('input2021-12-p23-17-_fight_data.csv',sep=',',header=0)
repo2 = pd.read_csv('testrun2021-12-p23-17-_fight_data.csv',sep=',',header=0)
data = np.array(repo)
data2 = np.array(repo2)

'''fig = plt.figure()
ax1 = fig.add_subplot(111)

def animate(i):
    ax1.clear()
    ax1.plot(data2[:,0], label = 'x')
    ax1.plot(data2[:,1], label = 'y')
    ax1.plot(data2[:,2], label = 'z')
    ax1.legend()

ani = animation.FuncAnimation(fig, animate, interval = 1000)'''
i = 19
j = 0
for k in range(500, 2480, 1):
    plt.scatter(k, data2[k,0], c ='b', label = 'x-lps')
    plt.scatter(k, data2[k,1], c ='r', label = 'y-lps')
    i = i + 1
    if i == 20:
        plt.scatter(k, data[j,0], c ='g', label = 'x-circle')
        plt.scatter(k, data[j,1], c ='m', label = 'y-circle' )
        j = j + 1
        i = 0 
plt.show()
