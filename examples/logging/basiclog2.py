"""
LPS
ts,sqn, distdiff, stddev, id0, id1 
Simple example that connects to the first Crazyflie found, logs the variable thomas need for his thesis
and saves it in a csv file. After 60s the application disconnects and exits.
"""
import logging
import time
from threading import Timer
import numpy as np

import cflib.crtp  # noqa
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.utils import uri_helper
import pdb
uri = uri_helper.uri_from_env(default='radio://0/100/2M/E7E7E7E725')

# Only output errors from the logging framework
logging.basicConfig(level=logging.ERROR)


class LoggingExample:
    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """
        arr0 = []
        arr0.append("ts,sqn, distdiff, id0, id1, stddev")
        np.savetxt('lps1-0_1-0.csv', arr0, delimiter=',', fmt='%s', comments='')

        self._cf = Crazyflie(rw_cache='./cache')

        # Connect some callbacks from the Crazyflie API
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        print('Connecting to %s' % link_uri)

        # Try to connect to the Crazyflie
        self._cf.open_link(link_uri)

        # Variable used to keep main loop occupied until disconnect
        self.is_connected = True

    def _connected(self, link_uri):
        log_rate = 10
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        print('Connected to %s' % link_uri)

        # The definition of the logconfig can be made before connecting
        range_log_conf0 = LogConfig(name='Lps0', period_in_ms=10)
        range_log_conf1 = LogConfig(name='Lps1', period_in_ms=10)
        range_log_conf2 = LogConfig(name='Lps2', period_in_ms=10)
        range_log_conf3 = LogConfig(name='Lps3', period_in_ms=10)
        range_log_conf4 = LogConfig(name='Lps4', period_in_ms=10)
        range_log_conf5 = LogConfig(name='Lps5', period_in_ms=10)
        range_log_conf6 = LogConfig(name='Lps6', period_in_ms=10)
        range_log_conf7 = LogConfig(name='Lps7', period_in_ms=10)

        range_log_conf0.add_variable('tdoa2new.sn-0', 'float')
        range_log_conf0.add_variable('tdoa2new.ts-0', 'float')
        range_log_conf0.add_variable('tdoa2.d7-0', 'float')
        range_log_conf0.add_variable('tdoa2new.id07', 'float')
        range_log_conf0.add_variable('tdoa2new.id10', 'float')
        range_log_conf0.add_variable('tdoa2new.stddev0', 'float')

        range_log_conf1.add_variable('tdoa2new.sn-1', 'float')
        range_log_conf1.add_variable('tdoa2new.ts-1', 'float')
        range_log_conf1.add_variable('tdoa2.d0-1', 'float')
        range_log_conf1.add_variable('tdoa2new.id00', 'float')
        range_log_conf1.add_variable('tdoa2new.id11', 'float')
        range_log_conf1.add_variable('tdoa2new.stddev1', 'float')

        range_log_conf2.add_variable('tdoa2new.sn-2', 'float')
        range_log_conf2.add_variable('tdoa2new.ts-2', 'float')
        range_log_conf2.add_variable('tdoa2.d1-2', 'float')
        range_log_conf2.add_variable('tdoa2new.id01', 'float')
        range_log_conf2.add_variable('tdoa2new.id12', 'float')
        range_log_conf2.add_variable('tdoa2new.stddev2', 'float')

        range_log_conf3.add_variable('tdoa2new.sn-3', 'float')
        range_log_conf3.add_variable('tdoa2new.ts-3', 'float')
        range_log_conf3.add_variable('tdoa2.d2-3', 'float')
        range_log_conf3.add_variable('tdoa2new.id02', 'float')
        range_log_conf3.add_variable('tdoa2new.id13', 'float')
        range_log_conf3.add_variable('tdoa2new.stddev3', 'float')

        range_log_conf4.add_variable('tdoa2new.sn-4', 'float')
        range_log_conf4.add_variable('tdoa2new.ts-4', 'float')
        range_log_conf4.add_variable('tdoa2.d3-4', 'float')
        range_log_conf4.add_variable('tdoa2new.id03', 'float')
        range_log_conf4.add_variable('tdoa2new.id14', 'float')
        range_log_conf4.add_variable('tdoa2new.stddev4', 'float')

        range_log_conf5.add_variable('tdoa2new.sn-5', 'float')
        range_log_conf5.add_variable('tdoa2new.ts-5', 'float')
        range_log_conf5.add_variable('tdoa2.d4-5', 'float')
        range_log_conf5.add_variable('tdoa2new.id04', 'float')
        range_log_conf5.add_variable('tdoa2new.id15', 'float')
        range_log_conf5.add_variable('tdoa2new.stddev5', 'float')
        
        range_log_conf6.add_variable('tdoa2new.sn-6', 'float')
        range_log_conf6.add_variable('tdoa2new.ts-6', 'float')
        range_log_conf6.add_variable('tdoa2.d5-6', 'float')
        range_log_conf6.add_variable('tdoa2new.id05', 'float')
        range_log_conf6.add_variable('tdoa2new.id16', 'float')
        range_log_conf6.add_variable('tdoa2new.stddev6', 'float')

        range_log_conf7.add_variable('tdoa2new.sn-7', 'float')
        range_log_conf7.add_variable('tdoa2new.ts-7', 'float')
        range_log_conf7.add_variable('tdoa2.d6-7', 'float')
        range_log_conf7.add_variable('tdoa2new.id06', 'float')
        range_log_conf7.add_variable('tdoa2new.id17', 'float')
        range_log_conf7.add_variable('tdoa2new.stddev7', 'float')
        # For looping
        self.log_configs = [\
            range_log_conf0,\
            range_log_conf1,\
            range_log_conf2,\
            range_log_conf3,\
            range_log_conf4,\
            range_log_conf5,\
            range_log_conf6,\
            range_log_conf7,\
        ]

        # Adding the configuration cannot be done until a Crazyflie is
        # connected, since we need to check that the variables we
        # would like to log are in the TOC.
        for conf in self.log_configs:
            self._cf.log.add_config(conf)
            # This callback will receive the data
            conf.data_received_cb.add_callback(self._stab_log_data)
            # This callback will be called on errors
            conf.error_cb.add_callback(self._stab_log_error)
            # Start the logging
            conf.start()
        
        # Start a timer to disconnect in 1s
        t = Timer(30, self._cf.close_link)
        t.start()
        

    def _stab_log_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print('Error when logging %s: %s' % (logconf.name, msg))

    def _stab_log_data(self, timestamp, data, logconf):
        arr = []
        arr3 = []
        """Callback from a the log API when data arrives"""
        for name, value in data.items():
            #pdb.set_trace()
            #print(f'{name}: {value:3.3f} ', end='')
            #self.arr.append([value])
            arr.append(value)
            #with open('./data/pos3.csv','a') as csvfile:
        arr3.append(arr)
        with open('lps1-0_1-0.csv','a') as csvfile:
            np.savetxt(csvfile, arr3, delimiter=',', fmt='%s', comments='')

        #print("logging...")

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the specified address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)
        self.is_connected = False


if __name__ == '__main__':
    # Initialize the low-level drivers
    cflib.crtp.init_drivers()

    le = LoggingExample(uri)

    # The Crazyflie lib doesn't contain anything to keep the application alive,
    # so this is where your application should do something. In our case we
    # are just waiting until we are disconnected.
    while le.is_connected:
        time.sleep(1)
