import time
from threading import Timer
import pdb
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.utils import uri_helper
import numpy as np


uri = uri_helper.uri_from_env(default='radio://0/100/2M/E7E7E7E705')

# convergence of kalman filtered data
# https://github.com/bitcraze/crazyflie-firmware/blob/f6c426431ed8db6d607c4a6b04ab30d091ca4d2e/src/modules/src/estimator_kalman.c#L713-L715
# the Kalman filter is fusing 6DOF (accelerometer + gyroscope) together with the raw LPS TDoA measurements.

# log blocks
# LPS - deck drivers LPSRDOA2TAG.C - distance difference
# IMU - sensorfusion.c ?? why dot product between g and a
# KALMAN OUTPUT - /master/src/modules/src
arr= []

def wait_for_position_estimator(scf):
    print('Waiting for estimator to find position...')
    pos_log_config = LogConfig(name='Kalman Variance', period_in_ms=500)
    pos_log_config.add_variable('kalman.varPX', 'float')
    pos_log_config.add_variable('kalman.varPY', 'float')
    pos_log_config.add_variable('kalman.varPZ', 'float')
    pos_log_config.add_variable('kalman.stateX', fetch_as="float")
    pos_log_config.add_variable('kalman.stateY', fetch_as="float")
    pos_log_config.add_variable('kalman.stateZ', fetch_as="float")

    arr= []

    var_y_history = [10] * 10
    var_x_history = [10] * 10
    var_z_history = [10] * 10

    threshold = 0.001
    start = time.time()
    with SyncLogger(scf, log_config) as logger:
        for log_entry in logger:
            # each instance of logger has all the info about each parameter of log config
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            print("{} {} {}".format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (
                    max_y - min_y) < threshold and (
                    max_z - min_z) < threshold:
                    end = time.time()
                    arr.append([data['kalman.varPX'], data['kalman.varPY'], data['kalman.varPZ'], data['kalman.stateX'], data['kalman.stateY'],data['kalman.stateZ'], end-start])
                    with open('./data/lps3.csv','a') as csvfile:
                        np.savetxt(csvfile,arr,delimiter=',',fmt='%s', comments='')
                    #print(arr)
                    print(" saved to the file & time taken to converge ", end-start)
                    break

def wait_for_lps_anchors(scf):
    log_rate = 10
    range_log_conf1 = LogConfig("seq0-5", log_rate) # sequence numbers 0-5
    range_log_conf2 = LogConfig("seq6-7", log_rate) # sequence numbers 6-7

    range_log_conf3 = LogConfig("distance difference 0-5", log_rate) # distance differece 0-5
    range_log_conf4 = LogConfig("distance difference 6-7", log_rate) # distance difference 6-7

    range_log_conf5 = LogConfig("timestamp0-5", log_rate) # timestamps 0-5
    range_log_conf6 = LogConfig("timestamp6-7", log_rate) # timestamps 6-7

    range_log_conf1.add_variable('tdoa2new.sn-0', fetch_as="float")
    range_log_conf1.add_variable('tdoa2new.sn-1', fetch_as="float")
    range_log_conf1.add_variable('tdoa2new.sn-2', fetch_as="float")
    range_log_conf1.add_variable('tdoa2new.sn-3', fetch_as="float")
    range_log_conf1.add_variable('tdoa2new.sn-4', fetch_as="float")
    range_log_conf1.add_variable('tdoa2new.sn-5', fetch_as="float")
    range_log_conf2.add_variable('tdoa2new.sn-6', fetch_as="float")
    range_log_conf2.add_variable('tdoa2new.sn-7', fetch_as="float")


    range_log_conf3.add_variable('tdoa.d0-1', fetch_as="float")
    range_log_conf3.add_variable('tdoa.d1-2', fetch_as="float")
    range_log_conf3.add_variable('tdoa.d2-3', fetch_as="float")
    range_log_conf3.add_variable('tdoa.d3-4', fetch_as="float")
    range_log_conf3.add_variable('tdoa.d4-5', fetch_as="float")
    range_log_conf3.add_variable('tdoa.d5-6', fetch_as="float")
    range_log_conf4.add_variable('tdoa.d6-7', fetch_as="float")
    range_log_conf4.add_variable('tdoa.d7-0', fetch_as="float")


    range_log_conf5.add_variable('tdoa2new.ts-0', fetch_as="float")
    range_log_conf5.add_variable('tdoa2new.ts-1', fetch_as="float")
    range_log_conf5.add_variable('tdoa2new.ts-2', fetch_as="float")
    range_log_conf5.add_variable('tdoa2new.ts-3', fetch_as="float")
    range_log_conf5.add_variable('tdoa2new.ts-4', fetch_as="float")
    range_log_conf5.add_variable('tdoa2new.ts-5', fetch_as="float")
    range_log_conf6.add_variable('tdoa2new.ts-6', fetch_as="float")
    range_log_conf6.add_variable('tdoa2new.ts-7', fetch_as="float")

    # For looping
    log_configs = [\
        range_log_conf2,\
        range_log_conf3,\
    ]
    
    print('Waiting for lps anchoes to measure distance...')

    try:
        _log.add_config(log_configs)
        # This callback will receive the data
        log_configs.data_received_cb.add_callback(_stab_log_data)
        # This callback will be called on errors
        log_configs.error_cb.add_callback(_stab_log_error)
        # Start the logging
        log_configs.start()
    except KeyError as e:
        print('Could not start log configuration,'
                '{} not found in TOC'.format(str(e)))
    except AttributeError:
        print('Could not add Stabilizer log config, bad configuration.')

def _stab_log_error(self, logconf, msg):
    """Callback from the log API when an error occurs"""
    print('Error when logging %s: %s' % (logconf.name, msg))

def _stab_log_data(self, timestamp, data, logconf):
    """Callback from a the log API when data arrives"""
    print(f'[{timestamp}][{logconf.name}]: ', end='')
    for name, value in data.items():
        print(f'{name}: {value:3.3f} ', end='')
    print()

    with SyncLogger(scf, logconf) as logger:
        for log_entry in logger:
            # each instance of logger has all the info about each parameter of log config
            data = log_entry[1]

            arr.append([data['kalman.varPX'], data['kalman.varPY'], data['kalman.varPZ'], data['kalman.stateX'], data['kalman.stateY'],data['kalman.stateZ'], end-start])
            print("saved to the array")

if __name__ == "__main__":
    # initializing the drivers
    cflib.crtp.init_drivers()
    """for i in range(10):
        with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
            cf = scf
            obj.wait_for_position_estimator(cf)"""
    with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
        cf = scf
        wait_for_lps_anchors(cf)
    
    with open('./data/pos1-logging.csv','a') as csvfile:
        np.savetxt(csvfile, arr, delimiter=',', fmt='%s', comments='')


