# The swarm takes off and flies a synchronous choreography before landing.
# The take-of is relative to the start position but the Goto are absolute.
import time
import numpy as np
import pdb
import math
import cflib.crtp
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.swarm import CachedCfFactory
from cflib.crazyflie.swarm import Swarm
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.positioning.motion_commander import MotionCommander

# Change uris according to your setup
URI0 = 'radio://0/100/2M/E7E7E7E725'
URI1 = 'radio://0/100/2M/E7E7E7E721'
URI2 = 'radio://0/100/2M/E7E7E7E705'
URI3 = 'radio://0/100/2M/E7E7E7E734'
URI4 = 'radio://0/100/2M/E7E7E7E703'
URI5 = 'radio://0/100/2M/E7E7E7E730'

# d: diameter of circle
# z: altitude
"""params0 = {'d': 1.0, 'z': 0.3}
params1 = {'d': 1.0, 'z': 0.3}
params2 = {'d': 0.0, 'z': 0.5}
params3 = {'d': 1.0, 'z': 0.3}
params4 = {'d': 1.0, 'z': 0.3}"""


uris = {
    #URI0,
    URI1,
    #URI2,
    #URI3,
    #URI4,
    #URI5,
}

"""params = {
    URI0: [params0],
    URI1: [params1],
    URI2: [params2],
    URI3: [params3],
    URI4: [params4],
}"""

def circular_trajectory():
    #siz = len(uri_s)
    setpoints_number = 100
    params = [[0 for y in range(setpoints_number)] for x in range(2)] #for x in range(siz)] for

    for j in range(setpoints_number):
            params[j, 0] = 0 # x
            params[j, 1] = 0 # y

    return params

def poshold(scf, t, z):
    steps = t * 10
    # get current pos
    a = get_position(scf)
    with SyncLogger(scf, a) as logger:
        for log_entry in logger:
                #data = log_entry[1]
                b = log_entry[1]['kalman.stateX']
                break
    print(b)
    
    for r in range(steps):
        scf.cf.commander.send_hover_setpoint(0, 0, 0, b)
        time.sleep(0.1)

def take_off(scf, height, velocity):
    print("taking off")
    with MotionCommander(scf) as mc:
        mc.up(height, velocity)

def land(scf):
    print("landing")
    with MotionCommander(scf) as mc:
        mc.land(0)

def run_sequence(scf):
    # giving time to converge instead of using the function
    try: 
        # take off
        take_off(scf,1, .5) # height, velocity

        # send in position setpoints
        #poshold(scf, 1, 1) # time, height
        # def send_position_setpoint(self, x, y, z, yaw):, crazyflie/commander.py
        # we have the scf
        #print("Current position "+scf.cf.get_estimated_positions())
        """for t in range(len(params)/len(params[1])):
            commander.send_setpoint(params[t, 0], params[t, 1], 0.5, 0)
            time.sleep(0.2)"""
        
        # go_to(self, x, y, z=DEFAULT, velocity=DEFAULT):, positioning/position_hl_commander.py

        # land    
        land(scf) # time

    except Exception as e:
        print('Exception: ', e)    

def get_position(scf):
    log_conf = LogConfig("positions", 10) 
    log_conf.add_variable('kalman.stateX', fetch_as="float")
    log_conf.add_variable('kalman.stateY', fetch_as="float")
    #log_conf.add_variable('kalman.stateZ', fetch_as="float")
    scf.cf.log.add_config(log_conf)
    return log_conf

log_rate = 10
# wait for kalman filter to converge
def wait_for_position_estimator(scf):
    print('Waiting for estimator to find position...')
    pos_log_config = LogConfig(name='Kalman Variance', period_in_ms=500)
    pos_log_config.add_variable('kalman.varPX', 'float')
    pos_log_config.add_variable('kalman.varPY', 'float')
    pos_log_config.add_variable('kalman.varPZ', 'float')

    var_y_history = [10] * 10
    var_x_history = [10] * 10
    var_z_history = [10] * 10

    threshold = 0.005
    with SyncLogger(scf, log_config) as logger:
        for log_entry in logger:
            # each instance of logger has all the info about each parameter of log config
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            #print("{} {} {}".format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (
                    max_y - min_y) < threshold and (
                    max_z - min_z) < threshold:
                    print('Threshold reached')
                    break

# callback position estimator convergence check
def reset_estimator(scf):
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    wait_for_position_estimator(cf)

# check if parameters are downloaded for a cf through comm
def wait_for_param_download(scf):
    while not scf.cf.param.is_updated:
        time.sleep(1.0)
    print('Parameter downloaded for', scf.cf.link_uri)


if __name__ == '__main__':
    cflib.crtp.init_drivers()
    # load data in params
    # params = circular_trajectory()
    print("swarming!!")
    factory = CachedCfFactory(rw_cache='./cache')
    with Swarm(uris, factory=factory) as swarm:
        # from library# allowing the KF to converge
        swarm.parallel(reset_estimator)
        print('Waiting for parameters to be downloaded')

        # using ten copters at once might clog the communication
        # so wait before we atart
        time.sleep(2.0)
        swarm.parallel(wait_for_param_download)
        #    print('Parameter downloaded for', scf.cf.link_uri)

        #a = swarm.get_estimated_positions()
        swarm.parallel(run_sequence) #args_dict=params)
