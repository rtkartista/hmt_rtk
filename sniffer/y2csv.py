from msilib import sequence
import yaml
import pdb
import csv

sequences = ["data", "lpp_data", "to", "type", "rxSys"]
seqs2 = ["from",'ts']
arr = ["d0","d1","d2","d3","d4","d5","d6","d7","from","s0","s1","s2","s3","s4","s5","s6","s7","ts0","ts1","ts2","ts3","ts4","ts5","ts6","ts7","timestamp"]
with open('data_loc8_sniffer.csv', 'w',newline='') as csvfile:
        writer = csv.writer(csvfile) 
        writer.writerow(arr)

file = open("yaml_loc8.yaml", "r")
dictionaries = yaml.load_all(file, Loader=yaml.CLoader)
for dictionary in dictionaries:
    arr = []
    #print(dictionary)
    for key, val in dictionary.items():
        if key not in sequences:
            if key in seqs2:
                arr.append(val)
            else:
                for vals in val:
                    arr.append(vals)
    with open('data_loc8_sniffer.csv', 'a', newline='') as csvfile:
        writer = csv.writer(csvfile) 
        writer.writerow(arr)