
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 17:35:16 2019

@author: leafj
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

df = pd.read_csv(r'C:\Users\leafj\Box Sync\SwarmsWeeklyMeetingAgendaNotes\OCT8demoData\10-07-19\LPS sniffer data\sniff_pipe_before_reboot_out.csv')
#df.drop(['Unnamed: 0'], axis=1)

# to, type have no useful information


#for key, grp in df.groupby('from'):
#    print('Making figure for {}.'.format(key))
#    plt.figure()
#    for i in range(len(df['from'].unique())):
#        if not key == i:
#            #[print('Adding plot for {}'.format(i))
#            grp['rxSys'] = grp['rxSys'] - grp['rxSys'].min()
#            d_column = 'd{}'.format(i)
#            grp_filter = grp[np.abs(stats.zscore(grp[d_column])) < 2]
#            plt.plot('rxSys', d_column, '.', data=grp_filter, label=i)
#            
#            print ('{} from {}: mean: {} std dev: {} variance: {}'.format(key, i, grp_filter[d_column].mean(), grp_filter[d_column].std(), grp_filter[d_column].var()))
#    
#    plt.title('Distance to anchor {} from all other anchors'.format(key))
#    #plt.ylim(33500, 34800)
#    plt.legend()
#    plt.show()
    
    
for key, grp in df.groupby('from'):
    print('Making figure for {}.'.format(key))
    plt.figure()
    for i in range(len(df['from'].unique())):
        if not key == i:
            #[print('Adding plot for {}'.format(i))
            ts_column = 'ts{}'.format(i)
            #grp[ts_column] = grp[ts_column] - grp[ts_column].min()
            d_column = 'd{}'.format(i)
            
            grp_filter = grp[np.abs(stats.zscore(grp[d_column])) < 2]
            plt.plot(range(len(grp_filter[d_column])), d_column, '.', data=grp_filter, label=i)
            
            #print ('{} from {}: mean: {} std dev: {} variance: {}'.format(key, i, grp_filter[d_column].mean(), grp_filter[d_column].std(), grp_filter[d_column].var()))
    
    plt.title('Distance to anchor {} from all other anchors'.format(key))
    #plt.ylim(33500, 34800)
    plt.legend()
    plt.show()
    
    
#for key, grp in df.groupby('from'):
#    print('Making seq figure for {}.'.format(key))
#    plt.figure()
#    for i in range(len(df['from'].unique())):
#        if not key == i:
#            #[print('Adding plot for {}'.format(i))
#            grp['rxSys'] = grp['rxSys'] - grp['rxSys'].min()
#            d_column = 'seq{}'.format(i)
#            grp_filter = grp[np.abs(stats.zscore(grp[d_column])) < 2]
#            plt.plot('rxSys', d_column, '.', data=grp_filter, label=i)
#            
#            #print ('{} from {}: mean: {} std dev: {} variance: {}'.format(key, i, grp_filter[d_column].mean(), grp_filter[d_column].std(), grp_filter[d_column].var()))
#    
#    plt.title('Sequence number to anchor {} from all other anchors'.format(key))
#    #plt.ylim(33500, 34800)
#    plt.legend()
#    plt.show()