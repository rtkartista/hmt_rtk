# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 16:45:41 2019

@author: leafj
"""

import yaml
import pandas as pd
import numpy as np
import os
import sys
# baseDir = 'data'
# os.chdir(baseDir)
filename = 'data.yaml'

with open(filename, 'r') as stream:
    docs = yaml.load_all(sys.stdin, Loader=yaml.CLoader)
    header = ['from','d0','d1','d2','d3','d4','d5','d6','d7','seq0','seq1','seq2','seq3','seq4','seq5','seq6','seq7','ts0','ts1','ts2','ts3','ts4','ts5','ts6','ts7','rxSys','to','ts','type']
    data = []
    #print('{} total entries.'.format(len(docs)))
    i = 0
    for doc in docs:
        entry = []
        entry.append(doc['from'])
        entry.extend(doc['distances'])
        entry.extend(doc['seqs'])
        entry.extend(doc['timestamps'])
        entry.append(doc['rxSys'])
        entry.append(doc['to'])
        entry.append(doc['ts'])
        entry.append(doc['type'])
        
        data.append(entry)
        i = i + 1
        if i % 250 == 0:
            print('Processed {} entries.'.format(i))
        
        
df = pd.DataFrame(data, columns=header)

df.to_csv('loc1.csv', index=False)


