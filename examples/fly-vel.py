import time
import csv
import datetime
import numpy as np
import pdb
import cflib.crtp
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.swarm import Swarm
from cflib.crazyflie.swarm import CachedCfFactory
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.positioning.motion_commander import MotionCommander

# Change uris and sequences according to your setup
URI1 = 'radio://0/100/2M/E7E7E7E725'
URI2 = 'radio://0/100/2M/E7E7E7E721'
"""URI3 = 'radio://0/115/2M/E7E7E7E715'
URI4 = 'radio://0/70/2M/E7E7E7E704'
URI5 = 'radio://0/70/2M/E7E7E7E705'
URI6 = 'radio://0/70/2M/E7E7E7E706'
URI7 = 'radio://0/70/2M/E7E7E7E707'
URI8 = 'radio://0/70/2M/E7E7E7E708'
URI9 = 'radio://0/70/2M/E7E7E7E709'
URI10 = 'radio://0/70/2M/E7E7E7E70A'"""

# radius 
# height
# starting_x 
# starting_y 
# degree offset for circle 
# direction around circle
param1 = {'r' : 0.2 , 'z' : -0.49 , 'x_s' : 1.87 , 'y_s' : 2.18 ,
         'x_l' : 1.9 , 'y_l' : 2.1 , 'offset' : 45 , 'dir' : 1}
param2 = {'r' : 0.7 , 'z' : 0.3 , 'x_s' : 3.85 , 'y_s' : 1.78 ,
         'x_l' : 3.85 , 'y_l' : 1.78 , 'offset' : 45 , 'dir' : 1}

params = {
    URI1: [param1],
    URI1: [param2],
}

# List of URIs, comment the one you do not want to fly
uris = {
    URI1,
    URI2,
}

log_rate = 10
# wait for kalman filter to converge
def wait_for_position_estimator(scf):
    print('Waiting for estimator to find position...')
    pos_log_config = LogConfig(name='Kalman Variance', period_in_ms=500)
    pos_log_config.add_variable('kalman.varPX', 'float')
    pos_log_config.add_variable('kalman.varPY', 'float')
    pos_log_config.add_variable('kalman.varPZ', 'float')

    var_y_history = [10] * 10
    var_x_history = [10] * 10
    var_z_history = [10] * 10

    threshold = 0.01
    with SyncLogger(scf, log_config) as logger:
        for log_entry in logger:
            # each instance of logger has all the info about each parameter of log config
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            #print("{} {} {}".format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (
                    max_y - min_y) < threshold and (
                    max_z - min_z) < threshold:
                    print('Threshold reached')
                    break

# callback position estimator convergence check
def reset_estimator(scf):
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    wait_for_position_estimator(cf)

# check if parameters are downloaded for a cf through comm
def wait_for_param_download(scf):
    while not scf.cf.param.is_updated:
        time.sleep(1.0)
    print('Parameter downloaded for', scf.cf.link_uri)

# generate trajectory and move sequnece
def take_off(cf, position):
    take_off_time = 0.1
    sleep_time = 0.05
    steps = int(take_off_time/sleep_time)
    vz = position[2]/take_off_time
    print('vz', vz)

    for i in range(steps):
        cf.commander.send_velocity_world_setpoint(0, 0, vz, 0)
        time.sleep(sleep_time)

def land(cf, position):
    landing_time = 1
    sleep_time = 1
    steps = int(landing_time/sleep_time)
    vz = -position[2]/landing_time
    print('vz', vz)

    for i in range(steps):
        cf.commander.send_velocity_world_setpoint(0, 0, vz, 0)
        time.sleep(sleep_time)
    cf.commander.send_setpoint(0, 0, 0, 0)
    time.sleep(sleep_time)

DEFAULT_HEIGHT = 0.5
def move_random(scf2):
    with MotionCommander(scf2, default_height=DEFAULT_HEIGHT) as mc:
        mc.up(0.2, velocity=0.1)
        print("up")
        mc.down(0.1)
        print("down")
        time.sleep(1)

        # We can also set the velocity
        mc.right(0.1, velocity=0.1)
        time.sleep(1)
        print("right")
        mc.left(0.1, velocity=0.1)
        time.sleep(1)
        print("left")
        # Or turn
        """mc.turn_left(90)
        print("turn")

        # We can do circles or parts of circles
        mc.circle_right(0.25, velocity=0.1, angle_degrees=180)
        time.sleep(1)
        print("circle")"""

        """# We can move along a line in 3D space
            mc.move_distance(-1, 0.0, 0.5, velocity=0.6)
            time.sleep(1)"""

def run_sequence(scf, para):
    try: 
        cf = scf.cf
        cf.param.set_value('flightmode.posSet', '1')
        r = para['r']
        z = para['z']
        x_s = para['x_s']
        y_s = para['y_s']
        x_l = para['x_l']
        y_l = para['y_l']
        offset = para['offset']
        direction = para['dir']
        """# take off
        take_off(cf, (x_s, y_s, z, 0)) # x, y, z, yaw"""

        # send in velocity commands
        move_random(cf)

        """# landing
        for t in range(10):
            cf.commander.send_setpoint(x_l, y_l, 0, int(z*1000))
            time.sleep(0.2)
        land(cf, (x_l, y_l,z,0))"""

    except Exception as e:
        print('Exception: ', e)

# print and save logged data
def position_callback(timestamp, data, logconf):
    x = data['kalman.stateX']
    y = data['kalman.stateY']
    z = data['kalman.stateZ']
    #print('pos:_({},_{},_{})'.format(x, y, z))

    with open('./testrun'+datetime.datetime.now().strftime('%Y-%m-p%d-%H-')+'_fight_data.csv','a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow([x, y, z, timestamp])
    csvfile.close()

# log position estimate
def start_position_printing(scf):
    log_conf = LogConfig("positions", log_rate) 
    log_conf.add_variable('kalman.stateX', fetch_as="float")
    log_conf.add_variable('kalman.stateY', fetch_as="float")
    log_conf.add_variable('kalman.stateZ', fetch_as="float")
    scf.cf.log.add_config(log_conf)
    log_conf.data_received_cb.add_callback(position_callback)
    log_conf.start()

if __name__ == '__main__':
    cflib.crtp.init_drivers(enable_debug_driver=False)

    factory = CachedCfFactory(rw_cache='./cache')
    with Swarm(uris, factory = factory) as swarm:

        # allowing the KF to converge
        swarm.parallel(reset_estimator)
        print('Waiting for parameters to be downloaded')

        # using ten copters at once might clog the communication
        # so wait before we atart
        time.sleep(2.0)
        swarm.parallel(wait_for_param_download)
        
        # print to csv and to the screen
        swarm.parallel(start_position_printing)
        time.sleep(0.5)

        # run the sequence to fly drones
        swarm.parallel(run_sequence, args_dict=params)